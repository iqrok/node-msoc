const msoc = require('./');

const encrypted = './example/sample.xlsx';
const decrypted = `./example/result-${Date.now()}.xlsx`;
const reencrypted = `./example/reencrypted-${Date.now()}.xlsx`;

const password = 'polisi';

const dec = msoc
	.input(encrypted)
	.output(decrypted)
	.password(password)
	.decrypt();

console.log('decryption :', dec);

const enc = msoc
	.input(decrypted)
	.output(reencrypted)
	.password(password)
	.debug()
	.encrypt();

console.log('encryption :', enc);
