#include <napi.h>
#include "msoc.h"

using namespace std;
using namespace Napi;

Value MSOC_decrypt(const CallbackInfo& info) {
	Env env = info.Env();

	if (!info[0].IsString() || !info[1].IsString() || !info[2].IsString()){
		TypeError::New(env, "Expected 3 Parameter(s) to be passed [ String, String, String ]")
			.ThrowAsJavaScriptException();

		return env.Null();
	}

	int err = 0;

	string input = info[0].ToString().Utf8Value();
	string output = info[1].ToString().Utf8Value();
	string key = info[2].ToString().Utf8Value();

	const char *inFile = input.c_str();
	const char *outFile = output.c_str();
	const char *pass = key.c_str();

	err = MSOC_decryptA(outFile, inFile, pass, NULL);

	return Number::New(env, err);
}

Value MSOC_encrypt(const CallbackInfo& info) {
	Env env = info.Env();

	if (!info[0].IsString() || !info[1].IsString() || !info[2].IsString()){
		TypeError::New(env, "Expected 3 Parameter(s) to be passed [ String, String, String ]")
			.ThrowAsJavaScriptException();

		return env.Null();
	}

	int err = 0;

	string input = info[0].ToString().Utf8Value();
	string output = info[1].ToString().Utf8Value();
	string key = info[2].ToString().Utf8Value();

	const char *inFile = input.c_str();
	const char *outFile = output.c_str();
	const char *pass = key.c_str();

	err = MSOC_encryptA(outFile, inFile, pass, NULL);

	return Number::New(env, err);
}

Value MSOC_errMessage(const CallbackInfo& info) {
	Env env = info.Env();

	if (!info[0].IsNumber()){
		TypeError::New(env, "Expected 1 Parameter(s) to be passed [ Number ]")
			.ThrowAsJavaScriptException();

		return env.Null();
	}

	int err = info[0].As<Number>();

	const char *msg = MSOC_getErrMessage(err);

	return String::New(env, msg);
}

Object Init(Env env, Object exports) {
	exports.Set(String::New(env, "MSOC_decrypt"), Function::New(env, MSOC_decrypt));
	exports.Set(String::New(env, "MSOC_encrypt"), Function::New(env, MSOC_encrypt));
	exports.Set(String::New(env, "MSOC_errMessage"), Function::New(env, MSOC_errMessage));
	return exports;
}

NODE_API_MODULE(msoc, Init);
