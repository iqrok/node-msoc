const MSOC = require('./build/Release/msoffice.node');

class _msoc{
	constructor(){
		this._clear();
	};

	/**
	 * clear and init variables
	 * @private
	 * @param {number} errno - error code to be returned
	 * @return {number} error code if provided, otherwise will return chain
	 * */
	_clear(errno = undefined){
		this._input = undefined;
		this._output = undefined;
		this._password = undefined;
		this._debug = undefined;

		return errno == undefined ? this : errno;
	};

	/**
	 * check whether all required inputs have been set
	 * @private
	 * @return {boolean} false if one or more inputs are undefined
	 * */
	_check(){
		if(this._input == null || this._output == null || this._password == null){
			return false;
		}

		return true;
	};

	/**
	 * encrypt or decrypt file using password
	 * @param {number} mode - 0 for decryption file; 1 for encryption
	 * @returns {Object} - error code. 0 means success
	 * */
	_process(mode){
		if(mode == undefined){
			throw 'Wrong mode should be 1 (enc) or 0 (dec)';
		}

		if(!this._check()){
			throw 'Please provide input, output, and password';
			return false;
		}

		const errno = mode === 0
			? MSOC.MSOC_decrypt(this._input, this._output,	this._password)
			: MSOC.MSOC_encrypt(this._input, this._output,	this._password);

		if(this._debug){
			errno == 0
				? console.log('PROCESS SUCCESS :', this.getErrorMessage(errno))
				: console.error('PROCESS FAILED :', this.getErrorMessage(errno));
		}

		return errno;
	};

	/**
	 * Get error messages from error code
	 * @return {string} error message
	 * */
	getErrorMessage(errno = undefined){
		return MSOC.MSOC_errMessage(errno)
	};

	/**
	 * Set input file to be encrypted/decrypted
	 * @param {string} path - file path
	 * @returns {Object} - returns this for chaining
	 * */
	input(path){
		this._input = path;
		return this;
	};

	/**
	 * Set output file to be  encrypted/decrypted
	 * @param {string} path - file path
	 * @returns {Object} - returns this for chaining
	 * */
	output(path){
		this._output = path;
		return this;
	};

	/**
	 * Set password to be used at encrypting/decrypting
	 * @param {string} path - file path
	 * @returns {Object} - returns this for chaining
	 * */
	password(key){
		this._password = key;
		return this;
	};

	/**
	 * set debug status for current chained process
	 * @param {boolean} status - debug status
	 * @returns {Object} - returns this for chaining
	 * */
	debug(status = true){
		this._debug = status ? true : false;
		return this;
	};

	/**
	 * decrypt input file to output file using password
	 * @returns {Object} - error code. 0 means success
	 * */
	decrypt(){
		const result = this._process(0);
		return this._clear(result);
	};

	/**
	 * encrypt input file to output file using password
	 * @returns {Object} - error code. 0 means success
	 * */
	encrypt(){
		const result = this._process(1);
		return this._clear(result);
	};
}

module.exports = new _msoc();
