{
  "targets": [{
    "target_name": "msoffice",
    "sources": [
		"./msoc.cc",
		"<(module_root_dir)/include/msoc.h"
	],
    "include_dirs": [
		"<!@(node -p \"require('node-addon-api').include\")",
		"<(module_root_dir)/.msoc/include",
		"<(module_root_dir)/.msoc/lib"
	],
	"link_settings": {
		"libraries": [
			"<(module_root_dir)/.msoc/lib/libmsoc.so",
			"<(module_root_dir)/.msoc/lib/libmsoc.a"
		],
		"library_dirs": [
			"<(module_root_dir)/.msoc/lib"
		]
	},
    "cflags": [
		"-fno-exceptions",
		"-lstdc++",
		"-lcrypto",
		"-Iinclude",
		"-Wall",
		"-Wextra",
		"-lcrypto",
		"-g",
		"-D_FILE_OFFSET_BITS=64",
		"-msse2",
		"-fno-operator-names",
		"-Wall",
		"-Wextra",
		"-Wformat=2",
		"-Wcast-qual",
		"-Wcast-align",
		"-Wwrite-strings",
		"-Wfloat-equal",
		"-Wpointer-arith",
		"#-Wswitch-enum",
		"-Wstrict-aliasing=2",
		"-lpthread"
	],
    "cflags_cc": [
		"-fno-exceptions",
		"-lstdc++",
		"-lcrypto",
		"-Iinclude",
		"-Wall",
		"-Wextra",
		"-lcrypto",
		"-g",
		"-D_FILE_OFFSET_BITS=64",
		"-msse2",
		"-fno-operator-names",
		"-Wall",
		"-Wextra",
		"-Wformat=2",
		"-Wcast-qual",
		"-Wcast-align",
		"-Wwrite-strings",
		"-Wfloat-equal",
		"-Wpointer-arith",
		"#-Wswitch-enum",
		"-Wstrict-aliasing=2",
		"-lpthread"
	],
	"ldflags": [
		"-lcrypto",
		"-lssl",
		"-lpthread"
	],
    "defines": [ 'NAPI_DISABLE_CPP_EXCEPTIONS' ]
  }]
}
